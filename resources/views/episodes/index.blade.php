@extends('layouts.main')

@section('extra_styles')

    @php
    //   $bg = "https://image.tmdb.org/t/p/w500/".$season['poster_path']."";
    $bg = '';
    @endphp
    <style>
    .parallax {
        /* The image used */
        /* background-image: url({{ $bg }}); */

        /* Set a specific height */
        /* min-height: 500px; */
        min-height: 100vh;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    </style>
@endsection('extra_styles')

@section('content')
<div class="parallax">
    <div class="container mx-auto mt-4 px-4 pt-16">
        {{-- <div class="grid-col grid-flow-col"> --}}
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-4">
            @foreach ($season['episodes'] as $episode)
                <div class="mb-4">
                    <img  src="{{ 'https://image.tmdb.org/t/p/w500/'. $episode['still_path'] }}" alt="poster" class="hover:opacity-75 ">
                    <span class="text-lg mt-2 hover:text-gray:300">{{ $episode['name'] }}</span>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
