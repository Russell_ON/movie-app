<div class="flex items-center">
    <div class="relative mt-3 md:mt-0" x-data="{isOpen: true}" x-on:click.away="isOpen = false">
        <input
            wire:model="search"
            type="search"
            class="bg-gray-800 rounded-full w-64 px-4 py-1 pl-8 focus:outline-none focus:shadow-outline"
            placeholder="Search"
            @focus="isOpen = true"
            @keydown.shift="isOpen = false"
        >

        <div wire:loading class="spinner top-0 right-0 pr-20 pt-2 mr-4 mt-3"></div>
        @if (strlen($search) > 2)
            <div class="absolute bg-gray-800 text-sm rounded w-64 mt-4" x-show.transition.opacity="isOpen">
                @if ($searchResults->count() > 0)
                    <ul>
                        @foreach ($searchResults as $result)
                            <li class="border-b border-gray-700">
                                @if (isset($result['title']))

                                <a href="{{ route('movies.show', $result['id']) }}" class="block hover:bg-gray-700 px-3 py-3 flex items-center" @if(isset($result->last)) @keydown.tab="isOpen = false"@endif>
                                    <img class="w-8" src="{{ 'https://image.tmdb.org/t/p/w92/'. $result['poster_path'] ?? '' }}" alt="">
                                    <span class="ml-4">{{ $result['title'] ?? $result['name']}}</span>
                                </a>
                                @else
                                <a href="{{ route('tv-show.show', $result['id']) }}" class="block hover:bg-gray-700 px-3 py-3 flex items-center" @if(isset($result->last)) @keydown.tab="isOpen = false"@endif>
                                    <img class="w-8" src="{{ 'https://image.tmdb.org/t/p/w92/'. $result['poster_path'] ?? '' }}" alt="">
                                    <span class="ml-4">{{ $result['name'] ?? $result['title']}}</span>
                                </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div class="px-3 py-3">No results for "{{ $search }}"</div>
                @endif
            </div>
        @endif

    </div>
</div>
