<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Movie App</title>
        <link rel="stylesheet" href="/css/main.css">
        <livewire:styles>
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

        @yield('extra_styles')
    </head>
    <body class="font-sans bg-gray-900 text-white">
        <nav class="border-b border-gray-800">
            <div class="container mx-auto p-4 flex flex-col md:flex-row items-center justify-between">
                <ul class="flex flex-col md:flex-row items-center">
                    <li>
                        <a href="{{ route('movies.index') }}">
                            <i class="fas fa-film"></i>Movie App
                        </a>
                    </li>

                    <li class="md:ml-16 md:mt-0">
                        <a href="{{ route('movies.index') }}" class="hover:text-gray-300">Movies</a>
                    </li>

                    <li class="md:ml-6 md:mt-0">
                        <a href="{{ route('tv-show.index') }}" class="hover:text-gray-300">TV Shows</a>
                    </li>

                </ul>
                <livewire:search-dropdown></livewire:search-dropdown>
            </div>
        </nav>
        @yield('content')
        <livewire:scripts>
    </body>
</html>
