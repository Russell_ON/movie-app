<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Http;
use Livewire\Component;



class SearchDropdown extends Component
{
    public $search = '';

    public function render()
    {
        $SearchResults = [];
        if(strlen($this->search) > 2){

            $SearchResultsMovies = Http::withToken(config('services.tmdb.token'))
            ->get('https://api.themoviedb.org/3/search/movie?query='. $this->search)
            ->json()['results'] ?? [];

            $SearchResultsTV = Http::withToken(config('services.tmdb.token'))
            ->get('https://api.themoviedb.org/3/search/tv?query='. $this->search)
            ->json()['results'] ?? [];

            $SearchResults = array_merge($SearchResultsTV, $SearchResultsMovies);

            $userInput = $this->search;
            usort($SearchResults, function ($a, $b) use ($userInput) {

                similar_text($userInput, $a['name'] ?? ( $a['title'] ?? ''), $percentA);
                similar_text($userInput, $b['name'] ?? ( $b['title'] ?? ''), $percentB);

                return $percentA === $percentB ? 0 : ($percentA > $percentB ? -1 : 1);
            });
        } else {
            $SearchResults = [];
        }

        return view('livewire.search-dropdown', [
            'searchResults' => collect($SearchResults)->take(7),
        ]);
    }
}
